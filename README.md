# Calculator


1) base1
    * Initial sequence: 1 + 2 =

2) base2
    * Sequence 2: [operand1] [operator] [operand2] =

3) base3
    * Sequence 3: [-operand1].[operand] [operator] [-operand2].[operand] =
    * Example: -8.5 + -1.5 =
    * Result: -10.0
    
4) base4
    ##### Implemented DEL button
    * Sequence: 1 2 4 + DEL DEL 3 - 2 4 DEL 3 =
    * i.e. 123 - 23 =
    * Result: 100.0

5) base5
   ##### Separated GUI and Logic
   ##### Improved code, so that dot cannot be used more than once for operands.
   
   
6) base6
   ##### (Current) Implemented multiple operations.
   * Sequence: 1 + 2 . 0 + - 5 . 1 =
   * i.e. 1 + 2 - 5.1 = 
   * Result = -2.0999999999999996
   ##### All other operators can also be used.
   
   ##### Please pass on the bug if you find any.
   ##### Thanks :)
